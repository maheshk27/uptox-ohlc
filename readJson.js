const readline = require('readline');
const request = require('request');
const fs = require('fs');

// create instance of readline
// each instance is associated with single input stream
let rlInterface = readline.createInterface({
    input: fs.createReadStream('trades.json')
});

let line_no = 0;

// event is emitted after each line
rlInterface.on('line', function (line) {
    line_no++;

    request.post(
        'http://localhost:3000/fsm/api/v1/stockData',
        { json: JSON.parse(line) },
        function (error, response, body) {
            console.log('data posted - ', line);
        }
    );
});

// end
rlInterface.on('close', function (line) {
    console.log('Total lines : ' + line_no);
});