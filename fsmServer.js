var cors = require('cors');
express = require('express');
bodyParser = require('body-parser');
request = require("request");
mCache = require('memory-cache');

var config = require('./config.json');
//Variables
const computeTimer = config.computeTimer;
var subscribedUsers = [];
mCache.put('barNum', 1);
fsmData = [];

//Routes
router = require('./fsmRoutes');

app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/fsm/api/v1', router);

//Setting max listeners to infinite.
process.setMaxListeners(0);
process.on('uncaughtException', function (err) {
    console.log('uncaughtException:', err.message);
    console.log(err.stack);
});

app.listen(config.port.fsmServer, function () {
    console.log('Listening on port:' + config.port.fsmServer);
    console.log('Welcome to FSM Server API');
});

//Socket Setup
const io = require('socket.io')(config.port.socket);
io.on('connection', client => {
    console.log('New Client Connected => ', client.id);

    client.on('subscribe', subscribeData => {
        console.log('New user subscribe!  => ', subscribeData);
        subscribeData.clientId = client.id
        if (subscribedUsers.indexOf(subscribeData.symbol) < 0)
            subscribedUsers.push(subscribeData);
        client.join(subscribeData.symbol);
    });
    client.on('disconnect', () => {
        subscribedUsers = subscribedUsers.filter(function (item) {
            return item.clientId !== client.id;
        });
        console.log('Client dis-connected => ', client.id);
    });
});

//Clear OHLC Data and increase Bar_Num
setInterval(() => {

    console.log("\n*******************" + new Date().toISOString() + "*******************\n");

    let prevBarNum = mCache.get('barNum');
    mCache.put('barNum', (prevBarNum + 1));

    let uniqueStocks = [];

    subscribedUsers.forEach(element => {
        if (uniqueStocks.indexOf(element.symbol) < 0) {
            uniqueStocks.push(element.symbol);
        }
    });

    console.log("subscribedUsers => ", subscribedUsers, ' \n');
    console.log("uniqueStocks => ", uniqueStocks, ' \n');

    uniqueStocks.forEach(element => {
        console.log("ohlc_notify | ", element, " | data count = ", (fsmData[element] ? fsmData[element].length : 0));
        console.log("ohlc_data | ", element, " | data => ", (fsmData[element]?fsmData[element]:[]));

        if (fsmData[element] && fsmData[element].length > 0) {
            io.in(element).emit('ohlc_notify', JSON.stringify(fsmData[element]))
        } else {
            io.in(element).emit('ohlc_notify', [])
        }
    });

    fsmData = [];
    mCache.put('fsmData', fsmData);
}, computeTimer);
