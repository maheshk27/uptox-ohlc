﻿require('rootpath')();

var apiRoutes = express.Router();

apiRoutes.get('/', function (req, res) {
    res.json({ message: 'Welcome to the FSM Compute Server API!' });
});

apiRoutes.post('/stockData', function (req, res) {

    let stockData = req.body;

    if (!fsmData) fsmData = [];

    var keyIndex = Object.keys(fsmData).indexOf(stockData.sym);

    if (keyIndex < 0) {
        fsmData[stockData.sym.toString()] = [];
        fsmData[stockData.sym.toString()].push(
            {
                "event":"ohlc_notify",
                "symbol":stockData.sym.toString(),
                "o": stockData.P,
                "h": stockData.P,
                "l": stockData.P,
                "c": 0,
                "v": stockData.Q,
                "bar_num": mCache.get('barNum'),
                "ts": stockData.TS2,
            }
        );
    }
    else {

        let prevStockData = fsmData[stockData.sym.toString()][fsmData[stockData.sym.toString()].length - 1];

        fsmData[stockData.sym.toString()].push(
            {
                "event":"ohlc_notify",
                "symbol":stockData.sym.toString(),
                "o": fsmData[stockData.sym.toString()][0].h,
                "h": (stockData.P > prevStockData.h) ? stockData.P : prevStockData.h,
                "l": (stockData.P < prevStockData.l) ? stockData.P : prevStockData.l,
                "c": stockData.P,
                "v": (prevStockData.v + stockData.Q),
                "bar_num": mCache.get('barNum'),
                "ts": stockData.TS2,
            }
        )

    }
    mCache.put('fsmData', fsmData);
    res.json({ "Data Received": req.body });
});

module.exports = apiRoutes;
