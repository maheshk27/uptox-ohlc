Design Criteria
================

Code is written in node.js language. Following are the worker threads as per the requirement.

Worker Thread 1 (readJson.js) => 
--------------------------------
        - To read the json file used fs (filestream) and readline npm modules. 
        - Read line interface (readline.createInterface()) reads the json file line by line.
        - Readed json object is posted on fsm server (http://localhost:3000/fsm/api/v1/stockData).
    
Worker Thread 2 (fsmServer.js) =>
---------------------------------
        - fsmServer is created using express on 3001 port.
        - fsmServer reads / captures the json via post on route "/fsm/api/v1/stockData".
        - Based on received feeds fsmServer calculates the OHLC of every stock / symbol.
        - memCache is used to maintain the bar_num and OHLC data. 
        - OHLC data is stored fsmData[] variable and cleared every 15 seconds via clearInterval.
        - Socket server is created on 3001 port. 
        - On subscribe event socket server maintains the user details (e.g. client-id) with subscribed stock / symbol.
        - On disconnect event user entry is removed (based on client-id) from the subscribed user details variable.
        - Based on subscribed users and unique subscribed stocks every 15 seconds socket server sends ohlc notifications to clients.
        - Socket-client is created in .html; which contains 2 events as subscribe and ohlc_notify.
        - For testing purpose we can pass stock / symbol in query parameter (e.g. socket-client.html?stock=ADAEUR)
        - On ohlc_notify upcoming ohlc data is displayed on browser.

Installation Guide
===================

1. git clone the project via git url - (https://gitlab.com/maheshk27/uptox-ohlc.git)

        git clone https://gitlab.com/maheshk27/uptox-ohlc.git

2. move to the project directory "uptox-ohlc" => cd uptox-ohlc;

3. install the node modules => npm install

4. computeTimer (15 Second), fsmServer and socket ports properties are maintained in config.json file.

5. Run / Start the fsm node server using below command.
        node fsmServer.js

6. Run / open the socket-client.html file in browser. 
        You can pass the any stock name in query parameter like below. 
        Incase stock name is not passed then Default stock is used 'XXBTZUSD'
        
        <Directory Path>/uptox-ohlc/socket-client.html?stock=[STOCK_NAME]

        e.g. file:///D:/uptox-ohlc/socket-client.html?stock=ADAEUR

7. Run / Start the json read worker thread in other command terminal to read json file using below command
        node readJson.js